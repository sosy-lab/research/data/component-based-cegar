# Technical Details of the C-CEGAR implementatino

As already stated, we implement C-CEGAR using CoVeriTeam.
The conceptual realization is depicted in the following figure:

```mermaid
graph TD
  check[CEX check] -- violation witness,<br />verdict --> rename2(["Rename({'verdict': 'validator_verdict'})"])
  rename2 --> ite2
  ite2{ITE} -- "[validator_verdict == TRUE]" --> refiner
  ite2 -- "[validator_verdict != TRUE]" --> repeat

  refiner[refiner] -- witness --> joiner
 
  verifier[Verifier] -- violation witness,<br />verdict --> rename1(["Rename({'verdict': 'verifier_verdict'})"])
  rename1 --> ite1
  ite1{ITE} -- "[verifier_verdict == FALSE]" --> check
  ite1 -- "[verifier_verdict != FALSE]" --> repeat

  joiner(["Joiner(Witness, {'witness', 'witness2'})"]) -- witness --> pred[witness2]
  pred --> joiner
  exit>exit]

  start>start]

  start --> verifier

  repeat{REPEAT} -- "[verifier_verdict != FALSE or validator_verdict != TRUE]" --> exit
  repeat{REPEAT} -- "[verifier_verdict == FALSE and validator_verdict == TRUE]" --> verifier

  joiner -- witness --> repeat
  
```

It contains most technical details, necessary to use the flexibility of CoVeriTeam,
that allows to exchange components easily.

