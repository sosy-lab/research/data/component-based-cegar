#!/bin/bash

DIR=$(dirname "$0")

sudo apt-get install -y \
	gcc-multilib \
	git \
	openjdk-11-jre-headless \
	openjdk-8-jre-headless \
	python-is-python3 \
	python3-pip 

sudo dpkg -i "$DIR"/setup/benchexec_3.10-1_all.deb
sudo gpasswd -a $USER benchexec

"$DIR"/setup/configure_cgroups.sh

pip3 install -r "$DIR"/setup/requirements.txt

git submodule update --init

# Remove BenchExec shipped with CPAchecker because we want to use the latest version
rm experiments/cpachecker-for-benchmarks/lib/python-benchmark/BenchExec-3.10-py3-none-any.whl

# Go into a subshell so that we go back into the correct directory after running install-tex.sh
(
cd setup
sudo ./install-tex.sh
)
