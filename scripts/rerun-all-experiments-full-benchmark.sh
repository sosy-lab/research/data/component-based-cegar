#!/bin/bash

# A script to rerun all the experiments, create the benchexec tables and generate the figures from the paper

cd "$(realpath $(dirname "$0")/..)"/experiments && make all BENCHDEF_SUFFIX=''
cd -
