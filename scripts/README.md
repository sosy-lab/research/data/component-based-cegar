
# Reproduction of Experimental Data

We provided a set of scripts, that rerun all experiments conducted 
during the paper's evaluation or to evaluate a single configuration of
C-CEGAR on the full dataset.


## Reproduction of Full Experimental Data
**WARNING**: Reproducing all results from the paper on a single machine with the
same resources given will take approx. 200 days! 

To re-run all experiments conducted, execute the script `rerun-all-experiments.sh`
in the folder `scripts`.
It will start Benchexec runs and generates an HTML table with the results,
as well as a csv file.
Afterwards, it generated all figures present in the paper.
The numbers from the table can be obtained using information in 
the file `README.md` in the `scripts` folder.
Please keep in mind that reported data may deviate on reproduction due to different
experimentation environments and measurement errors.

To rerun a single configuration, use the corresponding scripts from 
the `scripts` folder:

Script | used in RQ | Abstract Model Explorer  | Feasibility Checker | Precision Refiner | Exchange format
---|:---:|:---:|:---:|:---:|:---:
rerun-CPA-Pred.sh | RQ 1| - | - | - | internal
rerun-CPA-CPA-CPA-predmap-full-benchmark.sh|RQ 1 | CPAchecker | CPAchecker | CPAchecker | Predmap
rerun-CPA-CPA-CPA-predmap-long-full-benchmark.sh * |RQ 1 | CPAchecker | CPAchecker | CPAchecker | Predmap
rerun-CPA-CPA-CPA-full-benchmark.sh |RQ 2, RQ3 | CPAchecker | CPAchecker | CPAchecker |Invariant Witness
rerun-CPA-FShell-CPA-full-benchmark.sh|RQ 3 | CPAchecker | Fshell-Wit2Test | CPAchecker | Invariant Witnesses
rerun-CPA-UA-CPA-full-benchmark.sh|RQ 3 | CPAchecker | Ultimate Automizer | CPAchecker | Invariant Witnesses
rerun-CPA-CPA-UA-full-benchmark.sh|RQ 3.2 | CPAchecker | CPAchecker | Ultimate Automizer | Invariant Witnesses

*Using a timeout of 180 Minutes instead of 15.



### Obtaining the results:
After running all the experiments, the numbers from the tables can be obtained 
using the following links.  
The numbers in the cells are the one reported in the original paper.
The figures are generated in the folder `figures`


Table1 | correct overall | correct proof | correct alarm | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  
PRED| [2183](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=0(0*status*(category(in(correct))))) | [1343](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(correct))))) | [840](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(correct))))) | [0](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(wrong)))))  | [7](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(wrong))))) 
C-PRED| [2105](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=1(0*status*(category(in(correct)))))  | [1297](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(correct))))) | [808](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),1(0*status*(category(in(correct)))))| [0](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),1(0*status*(category(in(wrong))))) |[2](raw/overview-data-full.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(wrong))))) 
C-PRED-Wit| [ 1573](raw/overview-data-full.table.html#/table?hidden=0,3,4,5,6&filter=2(0*status*(category(in(correct)))))  |[978 ](raw/overview-data-full.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))))  |[ 595](raw/overview-data-full.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))))  |[0 ](raw/overview-data-full.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))))  | [1 ](raw/overview-data-full.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong))))) 



Table 2, RQ 3.1 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [1573 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=2(0*status*(category(in(correct)))) ) | [978 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [61 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 595](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [ 317](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))) )  | [1](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
FShell-witness2test| [ 612](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=3(0*status*(category(in(correct)))) ) | [515 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),3(0*status*(category(in(correct)))) )  | [0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),3(0*status*(category(in(correct)))),4(0*status*(category(notIn(correct)))) )  | [97 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),3(0*status*(category(in(correct)))) )  | [56 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(in(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)) ) 
UAutomizer| [1225 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=4(0*status*(category(in(correct)))) ) | [918 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),4(0*status*(category(in(correct)))) )  | [ 1](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | [307 ](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),4(0*status*(category(in(correct)))) )  | [ 20](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)) ) 


Table 2,RQ 3.2 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [1573 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&filter=2(0*status*(category(in(correct)))) ) | [978 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [ 301](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct))))  )  | [ 595](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [303 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))) )  | [1](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
UAutomizer  | [1016 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=5(0*status*(category(in(correct))))  ) | [ 716](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(correct))))  ) | [41 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))  )| [ 300](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),5(0*status*(category(in(correct))))  )| [ 6](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))  )| [ 0](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),5(0*status*(category(in(wrong))))  )| [1 ](raw/overview-data-full.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(wrong))))  )



##  Partial Reproduction of Experimental Data
To be able to reproduce some results, we created a much smaller 
benchmark set containing only 10 tasks, to allow for a reproduction
of at least some results within a few hours.
Please keep in mind that the small benchmark does not 
perfectly represent the full benchmark, hence some 
figures will look different.

To re-run all experiments on the reduced benchmark,
execute the script `rerun-all-experiments-reduced-benchmark.sh`
in the folder `scripts`.
It will start Benchexec runs and generates an HTML table with the results,
as well as a csv file.
Afterwards, it generated all figures present in the paper.
The numbers from the table can be obtained using information in 
the file `README.md` in the `scripts` folder.

To rerun a single configuration on the reduced benchmark, 
use the scripts with the same naming schema as in 3.1,
but with the suffix `reduced-benchmark`
the `scripts` folder.

The reduced benchmark set contains the following files:
- array-fpieqn3f.yml 
- bitvector/jain_1-1.c  
- floats-esbmc-regression/fmod.yml 
- float-benchs/addsub_float_inexact.yml
- list-properties/list_flag-1.yml 
- locks/test_locks_14-1.yml 
- loop-acceleration/const_1-1.yml
- loop-acceleration/simple_3-2.yml 
- loop-zilu/benchmark02_linear.yml
- loops/while_infinite_loop_1.yml  



### Obtaining the results:
After running all the experiments, the numbers from the tables can be obtained 
using the following links.  
The numbers in the cells are the one reported in the original paper.
Note that they will be much smaller using the reduced dataset.
The figures are generated in the folder `figures`


Table1 | correct overall | correct proof | correct alarm | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  
PRED| [2183](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=0(0*status*(category(in(correct))))) | [1343](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(correct))))) | [840](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(correct))))) | [0](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(wrong)))))  | [7](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(wrong))))) 
C-PRED| [2105](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=1(0*status*(category(in(correct)))))  | [1297](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(correct))))) | [808](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),1(0*status*(category(in(correct)))))| [0](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),1(0*status*(category(in(wrong))))) |[2](raw/overview-data-reduced.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(wrong))))) 
C-PRED-Wit| [ 1573](raw/overview-data-reduced.table.html#/table?hidden=0,3,4,5,6&filter=2(0*status*(category(in(correct)))))  |[978 ](raw/overview-data-reduced.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))))  |[ 595](raw/overview-data-reduced.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))))  |[0 ](raw/overview-data-reduced.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))))  | [1 ](raw/overview-data-reduced.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong))))) 



Table 2, RQ 3.1 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [1573 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=2(0*status*(category(in(correct)))) ) | [978 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [61 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 595](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [ 317](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))) )  | [1](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
FShell-witness2test| [ 612](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=3(0*status*(category(in(correct)))) ) | [515 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),3(0*status*(category(in(correct)))) )  | [0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),3(0*status*(category(in(correct)))),4(0*status*(category(notIn(correct)))) )  | [97 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),3(0*status*(category(in(correct)))) )  | [56 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(in(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)) ) 
UAutomizer| [1225 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=4(0*status*(category(in(correct)))) ) | [918 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),4(0*status*(category(in(correct)))) )  | [ 1](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | [307 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),4(0*status*(category(in(correct)))) )  | [ 20](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)) ) 


Table 2,RQ 3.2 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [1573 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&filter=2(0*status*(category(in(correct)))) ) | [978 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [ 301](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct))))  )  | [ 595](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [303 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct)))) )  | [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,false)),2(0*status*(category(in(wrong)))) )  | [1](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
UAutomizer  | [1016 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=5(0*status*(category(in(correct))))  ) | [ 716](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(correct))))  ) | [41 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))  )| [ 300](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),5(0*status*(category(in(correct))))  )| [ 6](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))  )| [ 0](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),5(0*status*(category(in(wrong))))  )| [1 ](raw/overview-data-reduced.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(wrong))))  )

