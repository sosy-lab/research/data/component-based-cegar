#!/bin/bash

# Install latex
sudo apt-get  install -y texlive-latex-recommended

# Additional packages
sudo mkdir /usr/share/texlive/texmf-dist/tex/latex/type1ec
sudo cp type1ec.sty /usr/share/texlive/texmf-dist/tex/latex/type1ec
sudo mkdir /usr/share/texlive/texmf-dist/tex/latex/type1cm
sudo cp type1cm.sty /usr/share/texlive/texmf-dist/tex/latex/type1cm
sudo texhash 
sudo mktexlsr 
