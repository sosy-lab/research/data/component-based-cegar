#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

rm -rf UAutomizer-linux
wget https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/uautomizer.zip -O uautomizer.zip
unzip -o uautomizer.zip
(
  cd UAutomizer-linux
  cp ../UA_witness_updater.py ./
  git apply ../uautomizer-wrapper.diff
  sed -i "1 i \[This is a modified version of UAutomizer's SV-COMP 22 release.\ncf. https://gitlab.com/sosy-lab/research/data/component-based-cegar/-/tree/main/uautomizer-modified\]\n" README

)
zip -r uautomizer.zip UAutomizer-linux
rm -rf UAutomizer-linux
