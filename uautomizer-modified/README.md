Run `./create-archive.sh` to download the SV-COMP '22 release of Ultimate Automizer
and adjust it with our modifications.
The resulting file will be `uautomizer.zip`
