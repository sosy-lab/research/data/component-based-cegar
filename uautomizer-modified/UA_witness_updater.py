#!/usr/bin/env python3

import argparse
import sys
import xml.etree.ElementTree as ET

debug = True


def findRoot(g):
    # find the initial node for first :
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        # print( node.tag, node.attrib)
        for nodeAttr in n:
            if nodeAttr.get('key') == 'entry' and nodeAttr.text == 'true':
                return n
    return None


def containsInvar(g):
    # find the initial node for first :
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        # print( node.tag, node.attrib)
        for nodeAttr in n:
            if nodeAttr.attrib.get('key') == 'invariant' and nodeAttr.text is not None:
                return True
    return False


# Pretty print
def prettyPrint(mapping):
    for key in mapping:
        fir = mapping[key]
        print(key.get('id'), "-->", "(", fir.get('id'), ")")


def prettyPrintEdges(mapping):
    for key in mapping:
        for edge in mapping[key]:
            print(key, "-->", "(", edge.get('source'), edge.get('target'), ")")


def prettyPrintList(l):
    res = ''
    for n in l:
        res = res + n.get('id') + ","
    return res


def computeSrtToTgt(g, name):
    if debug:
        print("Computing SrtToTrgt")
    res = {}
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        res[n.get('id')] = []
    for e in g.findall("{http://graphml.graphdrawing.org/xmlns}edge"):
        if e.get('source') in res:
            res[e.get('source')].append(e)
    return res


def computeNodeNames2Nodes(g):
    res = {}
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        res[n.get('id')] = n
    return res


def computeDeg(g):
    indeg = {}
    outdeg = {}
    # Initally, all nnodes have degree 0
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        indeg[n.get('id')] = 0
        outdeg[n.get('id')] = 0
    # Traverse all edges, increase indeg and outdeg for each niode
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}edge"):
        outdeg[n.get('source')] = outdeg[n.get('source')] + 1
        indeg[n.get('target')] = indeg[n.get('target')] + 1

    return indeg, outdeg


def sameSourceCodeLine(firstEdge, secEdge):
    if debug:
        print("comparing", firstEdge.get('id'), " and ", secEdge.get('id'))
    startMatch = False
    endMatch = False
    for attr1 in firstEdge.iter():
        for attr2 in secEdge.iter():

            if attr1.get('key') == 'startline' and attr2.get('key') == 'startline' and attr1.text == attr2.text:
                startMatch = True
            elif attr1.get('key') == 'endline' and attr2.get(
                    'key') == 'endline' and attr1.text == attr2.text and attr1.text == attr2.text:
                endMatch = True
    print(startMatch, endMatch)
    return startMatch and endMatch


def noInfos(nodeName, g):
    matched = False
    for n in g.findall("{http://graphml.graphdrawing.org/xmlns}node"):
        if n.get('id') == nodeName:
            matched = True
            if len(list(n)) == 0:
                return True
    return False


def isControlEdge(e):
    for attr in e.findall("{http://graphml.graphdrawing.org/xmlns}data"):
        if attr.get('key') == 'control':
            print(e.get('id'), "is a control edge")
            return True
    return False


def parse(argv):
    desc = "Update a witness produced by UA to be accepted by CPAchecker. This is WIP, as long as the corresponding ticket 481 at Ultimate framework si not fixed.\n "
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-V", "--version", action="version", version="0.1")
    parser.add_argument("-w", "--witness", required=True, help="The witness produced by UA")
    parser.add_argument("-t", "--target", default="updated-witness.graphml",
                        help="The path for the updated witness to be stored to. Default is current "
                             "directory, filename is 'updated-witness.graphml'")
    # Read arguments from the command line
    return parser.parse_args()


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parse(argv)
    firstPath, targetPath = args.witness, args.target
    execute(firstPath, targetPath)


def execute(firstPath, targetPath):
    tree1 = ET.parse(firstPath)
    root1 = tree1.getroot()
    graph1 = root1.find("{http://graphml.graphdrawing.org/xmlns}graph")

    if graph1 is None:
        print(str("The first graph is not formatted correctly, it does not contain an '{"
                  "http://graphml.graphdrawing.org/xmlns}graph'"))
        return 2

    # Mapping from xml Node in g1 to xml node in g2
    rootNode1 = findRoot(graph1)

    # Mapping from Name of source node to edge
    allEdges1 = computeSrtToTgt(graph1, 'g1')

    # first, check if both G1 and G2 contain invariants:
    g1containsInvars = containsInvar(graph1)

    if not g1containsInvars:
        print(
            "The witness produced by UA does not contain any invariant, hence we can simply return it and stop, as the CPAchecker cannot load any invariants anyway.")

    else:
        # compute a mapping from source of edge to edge
        nodeNamesToNodesG1 = computeNodeNames2Nodes(graph1)

        # Compute for each node (name of node) the indegree and outdegree
        indeg, outdeg = computeDeg(graph1)

        print(indeg)
        print(outdeg)

        toProcess = [rootNode1]
        processed = []
        removed = []

        prettyPrintEdges(allEdges1)

        while len(toProcess) > 0:
            if debug:
                print("To process:", prettyPrintList(toProcess))
                print("processed:", prettyPrintList(processed))
            node = toProcess.pop()
            if node not in processed:
                processed.append(node)
            updated = False
            # Take all successors of node and check their indeg:
            for outgoingEdge in allEdges1[node.get('id')]:
                print(outgoingEdge, outgoingEdge.get('id'))
                successorOfCur = outgoingEdge.get('target')
                print("successor is", successorOfCur)
                # Only proceed, if the indeg and outdeg of the successor is 1
                if indeg[successorOfCur] == 1 and outdeg[successorOfCur] == 1 and not isControlEdge(outgoingEdge):
                    # an endge can be merged, if the outgoing edge of successorOrCur and the edge leading from
                    # the current node to it are identical (meanign, that they have the same source code line)
                    # and that the successor of the current node has no informatino atteched
                    tempArr = allEdges1[successorOfCur]
                    print(tempArr)
                    noInf = noInfos(successorOfCur, graph1)
                    if sameSourceCodeLine(outgoingEdge, tempArr[0]) and noInf:
                        print("+++ Match +++ : ", outgoingEdge.get('id'))
                        # We have found a dublicated edge, hence removing the middle node
                        print("updating edge ", outgoingEdge.get('id'), " by replacing", successorOfCur, "with",
                              tempArr[0].get('target'))
                        outgoingEdge.set('target', tempArr[0].get('target'))
                        removed.append(successorOfCur)
                        updated = True
                        # Finally, we add the current node to be processed again, if not already present:
                        if not node in toProcess:
                            toProcess.append(node)
                        if not nodeNamesToNodesG1[successorOfCur] in processed:
                            processed.append(nodeNamesToNodesG1[successorOfCur])
                        continue
                if not nodeNamesToNodesG1[successorOfCur] in processed and not updated:
                    toProcess.append(nodeNamesToNodesG1[successorOfCur])
                else:
                    print("Already processed", successorOfCur)

        print(removed)

    ET.register_namespace("", "http://graphml.graphdrawing.org/xmlns")
    ET.register_namespace("ad", "http://www.w3.org/2001/XMLSchema-instance")
    ET.register_namespace("ab", "http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd")

    tree1.write(targetPath)
    print("\nThe merged result is stored at ", targetPath)


if __name__ == '__main__':
    sys.exit(main())
