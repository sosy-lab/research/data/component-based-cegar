#!/usr/bin/env python3
# %%
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedFormatter, NullLocator
from matplotlib import rc
from scipy.stats.stats import pearsonr, chisquare
from collections import namedtuple

# %%
CORRECT = "correct"
TIMEOUT = "timeout"
NOT_CORRECT = "not correct"

import os
root_dir = os.path.join(os.path.dirname(__file__), "..")
path = os.path.join(root_dir, "processed-output", "overview-data.table.csv")
outdir = os.path.join(root_dir, "processed-output")

def get_status_and_runtime(path, x_status=2, x_cputime=3, y_status=7, y_cputime=8, add_col=None):
    Row = namedtuple('Row', ['task', 'status', 'expected_verdict', 'cputime', 'additional_column'])

    def cputime_x(row):
        try:
            return float(row[x_cputime])
        except (IndexError, ValueError):
            return None
    def cputime_y(row):
        try:
            return float(row[y_cputime])
        except (IndexError, ValueError):
            return None
    def status_x(row):
        try:
            expected = row[1]
            if row[x_status].startswith(expected):
                return CORRECT
            elif row[x_status].startswith("TIMEOUT"):
                return TIMEOUT
            else:
                return NOT_CORRECT
        except IndexError:
            return NOT_CORRECT
    def status_y(row):
        try:
            expected = row[1]
            if row[y_status].startswith(expected):
                return CORRECT
            elif row[y_status].startswith("TIMEOUT"):
                return TIMEOUT
            else:
                return NOT_CORRECT
        except IndexError:
            return NOT_CORRECT
    def additional(row):
        if add_col is None:
            return None
        try:
            return float(row[add_col])
        except (IndexError, ValueError):
            return -1


    with open(path) as inp:
        rows = [c.strip().split('\t') for c in inp.readlines()[3:]]
        num_columns = max((len(r) for r in rows))
        rows = [r + [''] * (num_columns - len(r)) for r in rows]

    return [Row(r[0], status_x(r), r[1], cputime_x(r), additional(r)) for r in rows], \
            [Row(r[0], status_y(r), r[1], cputime_y(r), additional(r)) for r in rows]


def get_data(path, x_status=2, x_cputime=3, y_status=7, y_cputime=8, include_timeouts=False, add_col=None):
    def is_relevant(row_x, row_y):
        return (row_x.status == CORRECT \
                or (include_timeouts and row_x.status == TIMEOUT)) \
            and (row_y.status == CORRECT \
                or (include_timeouts and row_y.status == TIMEOUT))

    x, y = get_status_and_runtime(path, x_status, x_cputime, y_status, y_cputime, add_col=add_col)
    x_and_y = [(r_x, r_y) for r_x, r_y in zip(x, y) if is_relevant(r_x, r_y)]

    if add_col is not None:
        return [x.cputime if x.cputime else 900 for x, _ in x_and_y], \
            [y.cputime if y.cputime else 900 for _, y in x_and_y], \
            [x.additional_column for x,_ in x_and_y]
    
    else:
        return [x.cputime if x.cputime else 900 for x, _ in x_and_y], \
            [y.cputime if y.cputime else 900 for _, y in x_and_y]
    


def get_data_for_scatterplot(path, x_status=2, x_cputime=3, y_status=7, y_cputime=8, include_timeouts=False):
    return get_data(path, x_status=x_status , x_cputime=x_cputime, y_status=y_status, y_cputime=y_cputime, \
    include_timeouts = include_timeouts, add_col=None)
   

# %%
runtime_increase_and_num_iterations = list()

## Scatter plot to check if everythink is ok 
rc("font", **{"family": "sans-serif", "size": 14})
rc("text", usetex=True)
x, y, it = get_data(path, y_status=8, y_cputime=9, add_col=12)


for i,xi in enumerate(x):
    yi = y[i]
    iti = it[i]
    runtime_increase_and_num_iterations.append((float(yi /xi), iti))
 
 
data = np.asarray([t for t,t1 in runtime_increase_and_num_iterations])



# %%
print("In total {} points are present".format(data.shape))
print("Average is {}, Median is {}, \n Varianc is {} Standard deviation is {}\n\n"
      .format(np.average(data),np.median(data), np.var(data), np.std(data)))
for i in range(90,100):
    print("{}% of the correctly solved within a increase by {}".format(i, np.percentile(data, i)))

# %%
## Generate the Boxplot:


DECIMAL_FORMAT = ".1f"
from math import floor, log10
def sig_digits(number, intended_digits=2):
    number = round(number, -int(floor(log10(abs(number)))) + (intended_digits - 1))
    before_dec, after_dec = str(number).split('.')
    if len(before_dec) >= intended_digits and before_dec != '0':
        return before_dec
    if before_dec != '0':
        digits_after_dec = intended_digits - len(before_dec)
    else:
        # not correct, but fits for our values
        digits_after_dec = intended_digits
    if digits_after_dec <= 0:
        return before_dec
    return f'{number}'
        

fig, ax = plt.subplots(1,figsize=(3, 5) )

# Creating plot
bp = plt.boxplot(data,  vert=True,showfliers=False,whis=[1,99] )
plt.suptitle('')

median = np.median(data)
for element in [ 'whiskers' ]:
    for line in bp[element]:

        # Get the position of the element. y is the label you want
        (x_l, y),(x_r, _) = line.get_xydata()
        # Make sure datapoints exist 
        # (I've been working with intervals, should not be problem for this case)
        if not np.isnan(y): 
            x_line_center = x_r + 0.11
            x_right = x_line_center
            y_line_center = y +0.5
                # Since it's a line and it's horisontal
                # overlay the value:  on the line, from center to right
            ax.text(x_line_center, y_line_center, # Position
                        f'{sig_digits(y)}', # Value (3f = 3 decimal float)
                        verticalalignment='top', # Centered vertically with line 
                        fontsize=16, backgroundcolor="white")

for element in ['medians' ]:
    for line in bp[element]:

        # Get the position of the element. y is the label you want
        (x_l, y),(x_r, _) = line.get_xydata()
        # Make sure datapoints exist 
        # (I've been working with intervals, should not be problem for this case)
        if not np.isnan(y): 
            x_line_center = x_right
            y_line_center = y 
                # Since it's a line and it's horisontal
                # overlay the value:  on the line, from center to right
            ax.text(x_line_center, y_line_center, # Position
                        f'{sig_digits(y)}', # Value (3f = 3 decimal float)
                        verticalalignment='top', # Centered vertically with line 
                        fontsize=16, backgroundcolor="white")

for element in ['caps']:
    for line in bp[element]:
        # Get the position of the element. y is the label you want
        (x_l, y),(x_r, _) = line.get_xydata()
        print((x_l, y),(x_r, _))
        # Make sure datapoints exist 
        # (I've been working with intervals, should not be problem for this case)
        if  not np.isnan(y):
            x_line_center = x_l -0.28
            y_line_center = y # Since it's a line and it's horisontal
            # overlay the value:  on the line, from center to right
            ax.text(x_line_center, y_line_center, # Position
                    f'{sig_digits(y)}', # Value (3f = 3 decimal float)
                    verticalalignment='top', # Centered vertically with line 
                    fontsize=16, backgroundcolor="white")

plt.rcParams.update({'font.size': 16})
plt.xticks([])
plt.yticks([])
for dim in ("left", "right", "bottom", "top"):
	ax.spines[dim].set_visible(False)
# show plot
plt.savefig(os.path.join(outdir, "Figure10b.pdf"))




median = np.median(data)
upper_quartile = np.percentile(data, 75)
lower_quartile = np.percentile(data, 25)

iqr = upper_quartile - lower_quartile
upper_whisker =np.percentile(data, 99)
lower_whisker =np.percentile(data, 1)

num_outliers = (list(filter(lambda x: x > upper_whisker, data)))

print(("median \t\t: {:" + DECIMAL_FORMAT + "}").format(median))    
print(("upper_quartile \t: {:" + DECIMAL_FORMAT + "}").format(upper_quartile))    
print(("lower_quartile \t: {:" + DECIMAL_FORMAT + "}").format(lower_quartile))    
print(("iqr \t\t: {:" + DECIMAL_FORMAT + "}").format(iqr))
print(("upper_whisker \t: {:" + DECIMAL_FORMAT + "}").format(upper_whisker))    
print(("lower_whisker \t: {:" + DECIMAL_FORMAT + "}").format(lower_whisker))  
print("num outliers \t: {}".format(len(num_outliers)) )

num_outliers.sort()
print(num_outliers)

# %%
def create_scatter_plot(
    x_data,
    y_data,
    xlabel="",
    ylabel="",
    xlim=[0, 900],
    ylim=[0, 900],
    xticks=None,
    yticks=None,
    scale="log",
    target_file=None,
    helper_line=True,
    **kwargs,
):
    plt.clf()
    ax = plt.gca()
    ax.set_xlabel(xlabel, labelpad=10)
    ax.set_ylabel(ylabel, labelpad=10)
    if isinstance(xlim, int) or isinstance(xlim, float):
        xlim = [0, xlim]
    if isinstance(ylim, int) or isinstance(ylim, float):
        ylim = [0, ylim]
    minlim = min(xlim[0], ylim[0])
    maxlim = max(xlim[1], ylim[1])
    if scale == "log":
        ax.set_xlim(0.5 * minlim, maxlim * 2)
        ax.set_ylim(0.5 * minlim, maxlim * 2)
        ax.spines["left"].set_bounds(low=ylim[0], high=ylim[1])
        ax.spines["bottom"].set_bounds(low=xlim[0], high=xlim[1])
        ax.set_xscale("log")
        ax.set_yscale("log")
    else:
        ax.set_xlim(-int(maxlim * 0.1), maxlim * 1.1)
        ax.set_ylim(-int(maxlim * 0.1), maxlim * 1.1)
        ax.spines["left"].set_bounds(low=ylim[0], high=ylim[1])
        ax.spines["bottom"].set_bounds(low=xlim[0], high=xlim[1])
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    if not xticks:
        if scale == "log":
            xticks = [xlim[0], int(xlim[1] / 10), xlim[1]]
        else:
            xticks = [xlim[0], int(sum(xlim) / 2), xlim[1]]
    if not yticks:
        if scale == "log":
            yticks = [ylim[0], int(ylim[1] / 10), ylim[1]]
        else:
            yticks = [ylim[0], int(sum(ylim) / 2), ylim[1]]
    if xlim[0] not in xticks:
        xticks.insert(0, xlim[0])
    if xlim[1] not in xticks:
        xticks.append(xlim[1])
    if ylim[0] not in yticks:
        yticks.insert(0, ylim[0])
    if ylim[1] not in yticks:
        yticks.append(ylim[1])

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_minor_locator(NullLocator())
    ax.xaxis.set_major_formatter(FixedFormatter([str(t) for t in xticks]))
    ax.yaxis.set_major_formatter(FixedFormatter([str(t) for t in yticks]))
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.set_aspect("equal")
    plt.gcf().set_size_inches(3.5, 3.5)

    if helper_line:
        plt.plot(
            range(int(minlim), int(maxlim)),
            range(int(minlim), int(maxlim)),
            "-k",
            alpha=0.3,
        )
        if scale == "log":
            plt.plot(
                range(math.floor(minlim), math.ceil(maxlim)),
                range(math.floor(minlim) * 10, math.ceil(maxlim) * 10, 10),
                "--",
                alpha=0.3,
                color="black",
            )
            plt.plot(
                range(math.floor(minlim) * 10, math.ceil(maxlim) * 10, 10),
                range(math.floor(minlim), math.ceil(maxlim)),
                "--",
                alpha=0.3,
                color="black",
            )
    kwargs.setdefault("edgecolors", "blue")
    kwargs.setdefault("facecolors", "blue")
    kwargs.setdefault("linewidth", 1)
    kwargs.setdefault("marker", "+")
    ax.scatter(x_data, y_data, **kwargs)

    plt.tight_layout()
    if target_file:
        plt.savefig(target_file, dpi=150)
        return plt
    return plt




# %%

## Scatter plot to check if everythink is ok 
rc("font", **{"family": "sans-serif", "size": 14})
rc("text", usetex=True)
x, y = get_data_for_scatterplot(path, y_status=8, y_cputime=9)


print(len(x))
create_scatter_plot(x, y, xlim=[1, 900], ylim=[1, 900],
	xticks=[1, 10, 100, 900], yticks=[1, 10, 100, 900],
	ylabel=r"CPU time for \textsc{C-Pred} (s)", xlabel=r"CPU time for \textsc{Pred} (s)",
	scale="log", marker="o", edgecolors="green", facecolors="None", alpha=0.1,
	target_file=os.path.join(outdir, "Figure10a.pdf"))

# %%
# Scatter plot for RQ 2

x, y = get_data_for_scatterplot(path, x_status=8, x_cputime=9, y_status=14, y_cputime=15)
create_scatter_plot(y, x, 
    xlim=[1, 900], ylim=[1, 900], xticks=[1, 10, 100, 900], yticks=[1, 10, 100, 900],
    ylabel=r"CPU time for \textsc{C-PredWit} (s)", xlabel=r"CPU time for \textsc{C-Pred} (s)",
    scale="log", marker="o", edgecolors="green", facecolors="None", alpha=0.1,
    target_file=os.path.join(outdir, "Figure13.pdf")
)

# %%
## some statistics for the iterations

iterations_to_averages= dict()
total = len(runtime_increase_and_num_iterations)
print(total)
sum_ =0
for x,y in runtime_increase_and_num_iterations:
    if y in iterations_to_averages :
        iterations_to_averages[y].append(x)
    else:
        iterations_to_averages[y] = [x]

for k, v in sorted(iterations_to_averages.items(), reverse=False):
        t = np.asarray(v)
        sum_ = sum_ + len(v)
        print("For {} iterations ({} data )\t Seen ({}/{}={}%\t\t Average is {:.2f}, \t median {:.2f}"
              .format(k,len(v),sum_,total,sum_/total*100 ,  np.average(t), np.median(t) ) )

print(iterations_to_averages[0.0].sort())

# %%
## Plot for the average by number of iterations
def print_barplot_avg(iterations_to_data, runtime_increase_and_num_iterations, iterations_max=None, target_file=None):
    if iterations_max is None:
        iterations_max = max(iterations_to_data)

    num_data = len(runtime_increase_and_num_iterations)
    labels = list()
    x_val = list()
    hight = list()
    width = list()
    for k, v in sorted(iterations_to_data.items(), reverse=False):
            if int(k) < iterations_max:
                    t = np.asarray(v)
                    hight.append(np.average(t))
                    percentage_of_data = len(v) / num_data * 100
                    w = percentage_of_data
                    x_val.append(sum(width) + w/2 + 5*k)
                    width.append(w)
                    labels.append(int(k)+1)


    figure_width = len(labels)*0.75
    minimum_width = figure_width / 50
    fig, ax = plt.subplots(figsize=(figure_width,3))
    displayed_widths = [max(minimum_width, w) for w in width]
    ax.bar(x_val,hight,width=displayed_widths, align='center', edgecolor='none', color='gray')
    ax.set_xticks(x_val)
    ax.set_xticklabels(labels)
    ax.set_yticks([])
    ax.set_xlabel("Number of CEGAR iterations to solve task")
    ax.set_ylabel("Median factor of\n increase in run time")
    for dim in ("right", "top", "left"):
        ax.spines[dim].set_visible(False)
    for i, v in enumerate(hight):
        if i == 8:
            vspace = 0.7
        else:
            vspace = 0.3
        ax.text(x_val[i] ,v+vspace
        , "{:.1f}".format(v), color='black', ha='center',fontsize=14)

    plt.tight_layout()
    if target_file:
        plt.savefig(target_file, dpi=150)

print_barplot_avg(iterations_to_averages, runtime_increase_and_num_iterations, target_file=os.path.join(outdir, "Figure11-with-outliers-avg.pdf"))

# %%
def print_barplot(iterations_to_data, runtime_increase_and_num_iterations, iterations_max=None, target_file=None):
    if iterations_max is None:
        iterations_max = max(iterations_to_data)

    num_data = len(runtime_increase_and_num_iterations)
    labels = list()
    x_val = list()
    hight = list()
    width = list()
    for k, v in sorted(iterations_to_data.items(), reverse=False):
            if int(k) < iterations_max:
                    t = np.asarray(v)
                    hight.append(np.median(t))
                    percentage_of_data = len(v) / num_data * 100
                    w = percentage_of_data
                    x_val.append(sum(width) + w/2 + 5*k)
                    width.append(w)
                    labels.append(int(k)+1)


    figure_width = len(labels)*0.75
    minimum_width = figure_width / 50
    fig, ax = plt.subplots(figsize=(figure_width,3))
    displayed_widths = [max(minimum_width, w) for w in width]
    ax.bar(x_val,hight,width=displayed_widths, align='center', edgecolor='none', color='green')
    ax.set_xticks(x_val)
    ax.set_xticklabels(labels)
    ax.set_yticks([])
    ax.set_xlabel("Number of CEGAR iterations to solve task")
    ax.set_ylabel("Median factor of\n increase in run time")
    for dim in ("right", "top", "left"):
        ax.spines[dim].set_visible(False)
    for i, v in enumerate(hight):
        if i == 8:
            vspace = 0.7
        else:
            vspace = 0.3
        ax.text(x_val[i] ,v+vspace
        , "{:.1f}".format(v), color='black', ha='center',fontsize=14)

    plt.tight_layout()
    if target_file:
        plt.savefig(target_file, dpi=150)

print_barplot(iterations_to_averages, runtime_increase_and_num_iterations, iterations_max=10, target_file=os.path.join(outdir, "Figure11.pdf"))

# %%
print_barplot(iterations_to_averages, runtime_increase_and_num_iterations, target_file=os.path.join(outdir, "Figure11-with-outliers.pdf"))
print("Missing numbers on x-axis correspond to 0 verification tasks that can be solved with this number of CEGAR iterations")


