#!/bin/bash

## If no parameter is given or the first parameter is empty,
## the table is generated for the full results.
## If the first parameter is '-examples', the table is generated for the smaller
## example benchmark set.
BENCHDEF_SUFFIX=${1:-}

SCRIPT_DIR=$(dirname "$0")
ROOT_DIR="$SCRIPT_DIR"/..
RESULTS_DIR="$ROOT_DIR/results"


## These blocks always get the newest results file for each verifier/composition.
## - Begin -

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "pred${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.predmap-false-restart-after-refinement.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  PRED=`ls -t "${RESULTS_DIR}"/pred${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.predmap-false-restart-after-refinement.xml.bz2 | head -1`;
  echo -e "For Pred, using results file\n\t${PRED}"
else
  echo "Error: No results found for Pred${BENCHDEF_SUFFIX}. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-pred.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPRED=`ls -t "${RESULTS_DIR}"/c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-pred.xml.bz2 | head -1`;
  echo -e "For CPred, using results file\n\t${CPRED}"
else
  echo "Error: No results found for CPred${BENCHDEF_SUFFIX}. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-cpachecker_ref-cpachecker.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPREDWIT=`ls -t "${RESULTS_DIR}"/c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-cpachecker_ref-cpachecker.xml.bz2 | head -1`;
  echo -e "For CPredWit, using results file\n\t${CPREDWIT}"
else
  echo "Error: No results found for CPredWit${BENCHDEF_SUFFIX}. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-fshell_ref-cpachecker.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPREDWIT_CEX_FSHELL=`ls -t "${RESULTS_DIR}"/c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-fshell_ref-cpachecker.xml.bz2 | head -1`;
  echo -e "For CPredWit + feasibility checker FShell-witness2test, using results file\n\t${CPREDWIT_CEX_FSHELL}"
else
  echo "Error: No results found for CPredWit${BENCHDEF_SUFFIX} + feasibility checker FShell-witness2test. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-uautomizer_ref-cpachecker.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPREDWIT_CEX_UAUTO=`ls -t "${RESULTS_DIR}"/c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-uautomizer_ref-cpachecker.xml.bz2 | head -1`;
  echo -e "For CPredWit + feasibility checker UAutomizer, using results file\n\t${CPREDWIT_CEX_UAUTO}"
else
  echo "Error: No results found for CPredWit${BENCHDEF_SUFFIX} + feasibility checker UAutomizer. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-cpachecker_ref-uautomizer.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPREDWIT_REF_UAUTO=`ls -t "${RESULTS_DIR}"/c-cegar${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-predwit_cex-cpachecker_ref-uautomizer.xml.bz2 | head -1`;
  echo -e "For CPredWit + precision refiner UAutomizer, using results file\n\t${CPREDWIT_REF_UAUTO}"
else
  echo "Error: No results found for CPredWit${BENCHDEF_SUFFIX} + precision refiner UAutomizer. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

FOUNDRESULTS=`find "${RESULTS_DIR}"  -maxdepth 1 -name "c-cegar-long${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-pred-long.xml.bz2"`
if [ -n "$FOUNDRESULTS" ]; then
  CPRED_LONG=`ls -t "${RESULTS_DIR}"/c-cegar-long${BENCHDEF_SUFFIX}.????-??-??_??-??-??.results.c-pred-long.xml.bz2 | head -1`;
  echo -e "For CPredWit (longer time limit), using results file\n\t ${CPRED_LONG}"
else
  echo "Error: No results found for CPred${BENCHDEF_SUFFIX} with longer time limit. Please run the corresponding experiments (or 'cp raw-data/* results/') and try again."
  exit 1
fi

## - End blocks for getting latest result files for each verifier/composition -

# Generate the actual HTML and CSV tables with BenchExec's table-generator
table-generator --no-diff -x "$SCRIPT_DIR"/overview-table.xml --name overview-data -o "$ROOT_DIR/processed-output" "$PRED" "$CPRED" "$CPREDWIT" "$CPREDWIT_CEX_FSHELL" "$CPREDWIT_CEX_UAUTO" "$CPREDWIT_REF_UAUTO" "$CPRED_LONG"
