This artifact was tested on a VirtualBox virtual machine
with 5 cores and 12 GB of memory.
It requires about 20 GB of disk space.

After importing the Virtual Machine with the required resources and starting it,
open file `~/ICSE22AE_Decomposing_SoftwareVerification/README.html` to get more instructions.

VM username: icse22ae
VM password: icse22ae

We strongly encourage use of the virtual machine or use of Ubuntu 20.04.
Setup outside of these two options may require knowledge about configuration
of the Linux kernel (to enable CGroups v1).

Usage in docker containers is possible,
but requires more additional setup. Because of this, we do not encourage it.
