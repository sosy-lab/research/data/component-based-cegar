# Reproduction Package for Article "Decomposing Software Verification into Off-the-Shelf Components: An Application to CEGAR"

Package authors: Jan Haltermann and Thomas Lemberger

## **Contents**

This package supports the reuse of component-based CEGAR, introduced in [our article](ARTICLE.pdf)

"Dirk Beyer, Jan Haltermann, Thomas Lemberger, Heike Wehrheim:
Decomposing Software Verification into Off-the-Shelf Components: An Application to CEGAR.
Proc. ICSE 2022."

The package also supports the claims made in this article.
We assume that users of this package have read the corresponding research article.

All given commands must be executed on the command-line.
We assume that each given code block is executed from the directory of this README file.

## **Table of Contents**

1. [Setup](#1-setup)
2. [Reproduction of Figures and Tables](#2-reproduction-of-figures-and-tables)
3. [Reproduction of Experimental Data](#3-reproduction-of-experimental-data)
4. [Reuse: Local Usage and Modifying Configurations of C-CEGAR](#4-reuse-local-usage-and-modifying-configurations-of-c-cegar)
5. [Supplements](#5-supplements)

---

## **1. Setup**

A virtual machine that is ready-to-use is provided with the artifact.
If you use this VM, you can skip the full setup.

### **1.1 Requirements**

*Note:
For Ubuntu 20.04 (especially for the ICSE22AE-VM), all requirements can be installed and set up by running* `./setup.sh` .


If you do not use Ubuntu 20.04, the following software is needed:

- git
- Python 3.8
- Java 11
- Java 8

And the following python packages are needed:

- numpy 1.21
- matplotlib 3.5
- scipy 1.7

In addition, [Benchexec](https://github.com/sosy-lab/benchexec/)
needs to be set up correctly.

BenchExec requires CGroups v1.
See [`experiments/benchexec/doc/INDEX.md`](experiments/benchexec/doc/INDEX.md) for more information.

<br>

### **1.2 Smoke Testing the Tools**

You can check your CGroup setup for BenchExec and CoVeriTeam with this command:

```
$ python3 -m benchexec.check_cgroups
```

This will report warnings and exit with code 1 if something is missing. 
If the command runs without output and exits immediately, the setup is working.

---

## **2. Reproduction of Figures and Tables**

*As a reference, the original figures and tables used in the article
are contained in directory [`expected-output/`](expected-output/).*

To reproduce the article's claims and figures from our raw experimental data,
run the following commands:

```
$ cd experiments
$ cp raw-data/* results/
$ make figures
```

This will generate all figures from the article
and put them in [`experiments/processed-output/`](`experiments/processed-output`).

Figures are generated based on the latest available benchmark runs in `experiments/results/`.
The script output shows the files used, for example:

```
scripts/generate-tables.sh
For Pred, using results file
        scripts/../results/pred.2022-01-26_17-47-15.results.predmap-false-restart-after-refinement.xml.bz2
For CPred, using results file
        scripts/../results/c-cegar.2022-01-27_15-59-31.results.c-pred.xml.bz2
For CPredWit, using results file
        scripts/../results/c-cegar.2022-01-27_18-18-15.results.c-predwit_cex-cpachecker_ref-cpachecker.xml.bz2
For CPredWit + feasibility checker FShell-witness2test, using results file
        scripts/../results/c-cegar.2022-01-27_18-59-29.results.c-predwit_cex-fshell_ref-cpachecker.xml.bz2
For CPredWit + feasibility checker UAutomizer, using results file
        scripts/../results/c-cegar.2022-01-27_22-37-39.results.c-predwit_cex-uautomizer_ref-cpachecker.xml.bz2
For CPredWit + precision refiner UAutomizer, using results file
        scripts/../results/c-cegar.2022-01-28_01-01-53.results.c-predwit_cex-cpachecker_ref-uautomizer.xml.bz2
For CPredWit (longer time limit), using results file
         scripts/../results/c-cegar-long.2022-01-28_03-13-34.results.c-pred-long.xml.bz2
[...]
```

In addition to the figures from the article, two more figures are generated:

* `Figure11-with-outliers.pdf` shows the same information as `Figure11.pdf`, but includes all data, including outliers with a very high number of refinements.
* `Figure11-with-outliers-avg.pdf` shows the same data as `Figure11-with-outliers.pdf`, but uses the average instead of the median.

To reproduce the numbers from the tables, use the following links
**after** running the above commands.
Each link will redirect you to a pre-filtered table
that contains all benchmark runs falling into that catergory, for example,
all tasks with a correct proof.
The number of tasks is shown in the upper right corner of the table:

<img src="setup/images/task-display.png" alt="Example for number of tasks" />

The filter-criteria used are shown when you click on that number display:

<img src="setup/images/task-filters.png" width="500px" alt="Example for number of tasks" />

<br> 

### **Table 1:**

Table1: | correct overall | correct proof | correct alarm | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  
PRED| [3769](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=0(0*status*(category(in(correct))))) | [2556](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(correct))))) | [1213](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(correct))))) | [3](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),0(0*status*(category(in(wrong)))))  | [9](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),0(0*status*(category(in(wrong))))) 
C-PRED| [3524](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=1(0*status*(category(in(correct)))))  | [2450](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(correct))))) | [1074](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,false)),1(0*status*(category(in(correct)))))| 0 |[3](experiments/processed-output/overview-data.table.html#/table?hidden=2,3,4,5,6&filter=id(values(,true)),1(0*status*(category(in(wrong))))) 
C-PRED-Wit| [ 2854](experiments/processed-output/overview-data.table.html#/table?hidden=0,3,4,5,6&filter=2(0*status*(category(in(correct)))))  |[2110 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))))  |[ 744](experiments/processed-output/overview-data.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))))  |0 | [1 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,3,4,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong))))) 


### **Table 2:**

#### **RQ 3.1:**

C-PredWit + different feasibility checker (with precision refiner CPAchecker) 

RQ 3.1 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [2854 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=2(0*status*(category(in(correct)))) ) | [2110 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [494 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | [ 744](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [ 441](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(in(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(notIn(correct)))) )  | 0 | [1](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
FShell-witness2test| [ 1223](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=3(0*status*(category(in(correct)))) ) | [1226 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),3(0*status*(category(in(correct)))) )  | 0| [97 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),3(0*status*(category(in(correct)))) )  | [64 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(in(correct)))),4(0*status*(category(notIn(correct)))) )  | 0  | 0
UAutomizer| [1941 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=4(0*status*(category(in(correct)))) ) | [1614 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),4(0*status*(category(in(correct)))) )  | [ 4](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | [327 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),4(0*status*(category(in(correct)))) )  | [ 29](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),3(0*status*(category(notIn(correct)))),4(0*status*(category(in(correct)))) )  | 0| [ 1](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,5,6&filter=2(0*status*(category(in(wrong)))))  

#### **RQ 3.2:**

C-PredWit + different precision refiner (with feasibility checker CPAchecker)

RQ 3.2 | correct overall | correct proof |  correct proof unique | correct alarm |correct alarm unique | incorrect proof | incorrect alarm  
---|:---:|:---:|:---:|:---:|:---:  |:---:|:---:
CPAchecker | [2854 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&filter=2(0*status*(category(in(correct)))) ) | [2110 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(correct)))) )  | [ 709](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct))))  )  | [ 744](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,false)),2(0*status*(category(in(correct)))) )  | [436 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(in(correct)))),5(0*status*(category(notIn(correct)))) )  | 0| [1](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&filter=id(values(,true)),2(0*status*(category(in(wrong)))) ) 
UAutomizer  | [1739 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=5(0*status*(category(in(correct))))  ) | [ 1430](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(correct))))  ) | [29 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))  )| [ 309](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),5(0*status*(category(in(correct))))  )| [1](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,false)),2(0*status*(category(notIn(correct)))),5(0*status*(category(in(correct))))) | 0| [1 ](experiments/processed-output/overview-data.table.html#/table?hidden=0,1,3,4,6&sort=id,desc&filter=id(values(,true)),5(0*status*(category(in(wrong))))  )


---

## **3. Reproduction of Experimental Data**

We provide a Makefile to rerun all experiments presented in the article's evaluation.
We first show how to reproduce experimental data on the full and a partial benchmark set,
and then we show how to inspect the produced data.
Our scripts can also be used to evaluate a single configuration of C-CEGAR, if need be.
See the Sections 'Advanced' for this information.

<br>

### **3.1 Reproduction of Full Experimental Data**

**WARNING**: Reproducing all results from the article on a single machine
will take approx. 200 days! In addition, your (virtual) machine requires about 17 GB of memory
and at least 5 CPU cores.
Make sure to adjust the virtual machine accordingly.
But you likely want to skip this section and go straight to Section 3.2, which runs on less resources.

To re-run all experiments conducted, run the following command:

```
$ scripts/rerun-all-experiments.sh
```

It will start the experiment runs, generate an HTML and CSV table with the results,
and all figures present in the article.
Raw benchmark results are available in `experiments/results`.
Figures and tables are available in `experiments/processed-output`.

Please keep in mind that reported data may deviate on reproduction due to different
experimentation environments.

<br>

### **3.2 Partial Reproduction of Experimental Data**

To be able to reproduce some results in a feasible amount of time,
we've created a much smaller benchmark set of only 10 tasks.
Please keep in mind that this small benchmark set does not
perfectly represent the full set, hence figures will look different.

To re-run all experiments on the reduced benchmark,
run the following command:

```
$ scripts/rerun-all-experiments-reduced-benchmark.sh
```

It will start the experiment runs, generate an HTML and CSV table with the results,
and generate all figures present in the article.
It will run between four and five hours.

The output should look like the following:

```
cd CPAchecker-2.1.1-unix \
        && PYTHONPATH=../benchexec/ ../cpachecker-for-benchmarks/scripts/benchmark.py  -o ../results/ --read-only-dir / --hidden-dir /home --overlay-dir . --full-access-dir /sys/fs/cgroup --read-only-dir ../benchexec --read-only-dir ../sv-benchmarks ../benchmark-definitions/pred-examples.xml
2022-01-27 19:57:24 - INFO - Unable to find pqos_wrapper, please install it for cache allocation and monitoring if your CPU supports Intel RDT (cf. https://gitlab.com/sosy-lab/software/pqos-wrapper).

executing run set 'predmap-false-restart-after-refinement.Examples'     (10 files)
2022-01-27 19:57:25 - INFO - LXCFS is not available, some host information like the uptime leaks into the container.
19:57:25   array-fpi/eqn3f.yml                      false(unreach-call)         10.06    4.11
19:57:29   bitvector/jain_1-1.yml                   true                         9.20    3.93
19:57:33   floats-esbmc-regression/fmod.yml         ERROR (interpolation failed)   12.92    4.25
19:57:38   float-benchs/addsub_float_inexact.yml    ERROR (interpolation failed)    8.64    2.99
19:57:41   list-properties/list_flag-1.yml          false(unreach-call)         13.25    4.35
19:57:46   locks/test_locks_14-1.yml                true                        11.98    4.17
19:57:50   loop-acceleration/const_1-1.yml          true                         9.34    3.21
[...]
```

The script shows the exact command line used to run a benchmark set,
and subsequently prints the results of that run as a table:
The table shows the time of execution, the benchmark task, the produced result, the CPU time spent on that task, and the wall time spent on that task.
Result `false(unreach-call)` means
that a counterexample was found for the benchmark task (e.g., `array-fpi/eqn3f.yml`).
Result `true` means that the benchmark task was proven safe (e.g., `bitvector/jain_1-1.yml`).
Results `unknown`, `ERROR` and `TIMEOUT` have the expected meaning.


After the command finishes, the final results (figures and HTML and CSV table) are available in `experiments/processed-output`.

#### **Description of the reduced benchmark**
The reduced benchmark set contains ten tasks from the 8437 tasks used
for the evaluation.
They are taken from different sub-categories used in the SV-COMP 
(`Arrays`, `BitVectors`, `Loops`, `Heap` and `SoftwareSystems`).  
We select tasks that represent the findings from the paper as good as possible.
Hence, some tasks cannot be solved by a certain C-CEGAR configuration 
(e.g. C-Pred with CPAchecker as precision refiner computes a solution
for `bitvector/jain_1-1.c`, but C-PRED using UAutomizer does not compute
a solution).

<br>

### **3.3 Description of Result Files**

The above scripts produce two types of results:
The final, processed data, written to `experiments/processed-output`,
and raw experiment data, written to `experiments/results/`.

#### **Processed data (directory `experiments/processed-output`)**

Directory `experiments/processed-output` contains one PDF for each
Figure presented in our article.
It also contains result tables `overview-data.table.html` and
`overview-data.table.html`.
These provide a convenient way to inspect the experimental results in detail.


#### **Advanced: Raw experiment data (directory `experiments/results`)**

The files for each benchmark run are grouped by the benchmark-definition name
and the timestamp of experiment execution: `BENCHDEF.TIMESTAMP.*`,
for example, for a benchmark run of Pred on the full benchmark set:

```
pred.2022-02-08_15-10-47.files/
pred.2022-02-08_15-10-47.logfiles.zip/
pred.2022-02-08_15-10-47.results.*.xml.bz2
```

There are three types of raw data, but only the last, result files, are further processed.

The directory `.files` contains, for each executed benchmark run, the result files
produced by that run. This includes all predmaps and witnesses produced during execution,
and for C-CEGAR compositions the individual output logs of each run tool.
For space reasons, these files are always archived in individual files `RunResult*.zip`
that may be empty if no files were created by the experiment run.

The file `.logfiles.zip` contains, for each executed benchmark run,
the output log produced by the top-level tool.
For Pred, this is the output log of CPAchecker.
For all other configurations, this is the output log of CoVeriTeam.

The result files `*.xml.bz2` contain the benchmark measurements for each individual benchmark task.
An example file name for a result file is
`c-cegar.2022-02-08_17-28-46.results.c-pred.ReachSafety-Arrays.xml.bz2`.
This file name contains, next to the benchmark-definition name and the timestamp,
the used composition name (`c-pred`) and the subcategory of the benchmark (`ReachSafety-Arrays`).
The result files without a subcategory, e.g.,
`c-cegar.2022-02-08_17-28-46.results.c-pred.xml.bz2`,
contain all benchmark results - because of this, these are usually the most interesting.
The measurements in the result files can be used to generate HTML and CSV tables
with tool `experiments/benchexec/bin/table-generator`.
We also generate our figures from a CSV table that is produced from table-generator.

<br>

### **3.4 Advanced: Running individual benchmarks**

To rerun a single composition, use the corresponding Make targets:

Target | used in RQ | Abstract Model Explorer  | Feasibility Checker | Precision Refiner | Exchange format
---|:---:|:---:|:---:|:---:|:---:
pred | RQ 1| - | - | - | internal
c-pred|RQ 1 | CPAchecker | CPAchecker | CPAchecker | Predmap
c-pred*|RQ 1 | CPAchecker | CPAchecker | CPAchecker | Predmap
c-predwit_cex-cpachecker_ref-cpachecker|RQ 2, RQ3 | CPAchecker | CPAchecker | CPAchecker |Invariant Witness
c-predwit_cex-fshell_ref-cpachecker|RQ 3 | CPAchecker | Fshell-Wit2Test | CPAchecker | Invariant Witnesses
c-predwit_cex-uautomizer_ref-cpachecker|RQ 3 | CPAchecker | Ultimate Automizer | CPAchecker | Invariant Witnesses
c-predwit_cex-cpachecker_ref-uautomizer|RQ 3.2 | CPAchecker | CPAchecker | Ultimate Automizer | Invariant Witnesses

*Using a timeout of 180 minutes instead of 15.

For example, to run the full experiments for Pred and C-Pred:

```
$ cd experiments && make pred c-pred
```

To rerun a composition on the reduced benchmark,
specify the additional variable `BENCHDEF_SUFFIX=-examples`.

For example, to run the small benchmark set for C-PredWit
and C-PredWit with feasibility checker UAutomizer:

```
$ cd experiments && make BENCHDEF_SUFFIX=-examples \
    c-predwit_cex-cpachecker_ref-cpachecker \
    c-predwit_cex-uautomizer_ref-cpachecker
```


---

## **4. Reuse: Local Usage and Modifying Configurations of C-CEGAR**

Our implementation of C-CEGAR relies on CoVeriTeam.
The latest version of CoVeriTeam is available open source [on GitLab](https://gitlab.com/sosy-lab/software/coveriteam).
A explanation on the technical realization of C-CEGAR can be
found [in this file](doc/cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt).
A general introduction to CoVeriTeam compositions
and CoVeriTeam's domain-specific language CVT is available in [`experiments/coveriteam/doc/index.md`](experiments/coveriteam/doc/index.md)

All CoVeriTeam compositions and tool definitions can be found in
`experiments/coveriteam/examples/Component-based_CEGAR/`.

Composition descriptions end on file suffix `.cvt`.
The filename of each CVT file describes the exact composition defined by that file:
`cCegar-<exchange format>_cex-<feasibility checker>_ref-<precision refiner>.cvt`.
The abstract model explorer is omitted in the file name, because this is always CPAchecker with predicate analysis.

For instance, the composition using invariant witnesses as exchange format,
and CPAchecker as both Feasibility Checker and Precision Refiner (C-PredWit, used in RQ1),
is realized in file
`cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt`.

### **4.1 Local Usage of existing compositions**

To execute the composition `cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt`
for a single file, e.g. `experiments/sv-benchmarks/bitvector/jain_1-1.c`,
run the following from this README's directory:

```
$ cd experiments/coveriteam
$ bin/coveriteam \
        examples/Component-based_CEGAR/cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt \
        --data-model ILP32 \
        --input specification_path=../sv-benchmarks/c/properties/unreach-call.prp \
        --input program_path=../sv-benchmarks/c/bitvector/jain_1-1.c \
        --cache-dir used-tools/ \
        --no-cache-update
```

*To get a list of all available commands, run `bin/coveriteam --help`.*

Explanation of used parameters:

* `examples/Component-based_CEGAR/cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt` is the CoVeriTeam composition to run.
* `--data-model ILP32` specifies that a 32bit data model should be assumed. If verification should assume a 64bit data model, use `--data-model LP64`.
* `--input specification_path=../sv-benchmarks/c/properties/unreach-call.prp` sets the program specification to verify the program against.
* `--input program_path=../sv-benchmarks/c/bitvector/jain_1-1.c` sets the program to verify.
* `--cache-dir used-tools/` specifies the directory where the tools used in the composition are stored.
* `--no-cache-update` disables the automatic update of the cache directory. This requires internet access.

The above CoVeriTeam command will create the following output:

```
WARNING:root:Warning: No version specified for actor cpachecker-predicate-no-cegar. Taking the first version from the actor definition file.
You can specify the tool's version as a third parameter in the call to ActorFactory.create().
WARNING:root:Warning: No version specified for actor cpa-seq-validate-violation-witnesses. Taking the first version from the actor definition file.
You can specify the tool's version as a third parameter in the call to ActorFactory.create().
WARNING:root:Warning: No version specified for actor cpachecker-predicate-craig-interpolation. Taking the first version from the actor definition file.
You can specify the tool's version as a third parameter in the call to ActorFactory.create().
WARNING:root:Cputime measured by wait was 8.285987, cputime measured by cgroup was only 0.651761136, perhaps measurement is flawed.
Found invar None in scope None at node N14
Found invar None in scope None at node N15
Considering <Element '{http://graphml.graphdrawing.org/xmlns}data' at 0x7f8b7cab0950> for <Element '{http://graphml.graphdrawing.org/xmlns}node' at 0x7f8b7cab0900>
Got ( ( y % 2 ) == 1 ) || ( ( y == 1 ) && ( ( y % 2 ) == 1 ) ) for <Element '{http://graphml.graphdrawing.org/xmlns}node' at 0x7f8b7cab0900>
Found invar ( ( y % 2 ) == 1 ) || ( ( y == 1 ) && ( ( y % 2 ) == 1 ) ) in scope main at node N24
Found invar None in scope None at node N5
Found invar None in scope None at node N8
Found invar None in scope None at node N10
Found invar None in scope None at node N4
Found invar None in scope None at node N1
Found invar None in scope None at node N14
Found invar None in scope None at node N15
Considering <Element '{http://graphml.graphdrawing.org/xmlns}data' at 0x7f8b7cabbc20> for <Element '{http://graphml.graphdrawing.org/xmlns}node' at 0x7f8b7cabbbd0>
Got ( y == 1 ) for <Element '{http://graphml.graphdrawing.org/xmlns}node' at 0x7f8b7cabbbd0>
Found invar ( y == 1 ) in scope main at node N24
Found invar None in scope None at node N5
Found invar None in scope None at node N8
Found invar None in scope None at node N10
Found invar None in scope None at node N4
Found invar None in scope None at node N1
{'witness': 'cvt-output/435eb073-711b-4baa-b48b-8857ed06d41f/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output/a927e869-46bf-455f-a0e0-1f789427747f.graphml', 'verdict': 'true', 'verifier_verdict': 'true', 'validator_verdict': 'true', 'witness_old': 'cvt-output/435eb073-711b-4baa-b48b-8857ed06d41f/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output/a927e869-46bf-455f-a0e0-1f789427747f.graphml'}
```

It prints invariants found in the produced invariant witnesses,
and the last line shows the final artifacts produced by the analysis.
`'verdict': 'true'` says that the program under analysis is correct.
Value `'witness'` links to the file of the final invariant witness used for abstract model exploration
(in our case, file `cvt-output/435eb073-711b-4baa-b48b-8857ed06d41f/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output/a927e869-46bf-455f-a0e0-1f789427747f.graphml`)

Each CoVeriTeam run produces a new output directory `cvt-output/SOME_UNIQUE_HASH`.
For ease of use, the symbolic link `lastexecution` will point to the output directory of the last execution.
Each run's output directory contains:

* file `execution_trace.xml`, which describes the order of individual actor executions. 
  From this, it is visible which tool was executed in which order.
* subdirectories for each tool, for example `cpachecker-predicate-no-cegar/` or `cpachecker-predicate-craig-interpolation/`.
  These subdirectories contain a number of unique hashes, each representing one execution
  of the corresponding tool. Here you can find the individual output logs of the tool
  and generated artifacts (predmap or invariant witness as .graphml), if any.

For reference, the output directory for the above run contains the following files:

```
$ find lastexecution/
lastexecution/cpachecker-predicate-no-cegar
lastexecution/cpachecker-predicate-no-cegar/4eba3a58-7839-405d-aabb-729332a0c37e
lastexecution/cpachecker-predicate-no-cegar/4eba3a58-7839-405d-aabb-729332a0c37e/output
lastexecution/cpachecker-predicate-no-cegar/4eba3a58-7839-405d-aabb-729332a0c37e/output/witness.graphml
lastexecution/cpachecker-predicate-no-cegar/4eba3a58-7839-405d-aabb-729332a0c37e/output.txt
lastexecution/cpachecker-predicate-no-cegar/f2376f09-7145-4da2-8094-6f78a1952f9a
lastexecution/cpachecker-predicate-no-cegar/f2376f09-7145-4da2-8094-6f78a1952f9a/output
lastexecution/cpachecker-predicate-no-cegar/f2376f09-7145-4da2-8094-6f78a1952f9a/output/witness.graphml
lastexecution/cpachecker-predicate-no-cegar/f2376f09-7145-4da2-8094-6f78a1952f9a/output.txt
lastexecution/cpachecker-predicate-no-cegar/66412ae7-71bb-47fc-83bf-8a2fa2c80dee
lastexecution/cpachecker-predicate-no-cegar/66412ae7-71bb-47fc-83bf-8a2fa2c80dee/output
lastexecution/cpachecker-predicate-no-cegar/66412ae7-71bb-47fc-83bf-8a2fa2c80dee/output/witness.graphml
lastexecution/cpachecker-predicate-no-cegar/66412ae7-71bb-47fc-83bf-8a2fa2c80dee/output.txt
lastexecution/execution_trace.xml
lastexecution/cpa-seq-validate-violation-witnesses
lastexecution/cpa-seq-validate-violation-witnesses/75165138-5aac-420d-83bb-7370e8c21252
lastexecution/cpa-seq-validate-violation-witnesses/75165138-5aac-420d-83bb-7370e8c21252/output
lastexecution/cpa-seq-validate-violation-witnesses/75165138-5aac-420d-83bb-7370e8c21252/output/witness.graphml
lastexecution/cpa-seq-validate-violation-witnesses/75165138-5aac-420d-83bb-7370e8c21252/output.txt
lastexecution/cpa-seq-validate-violation-witnesses/0fc94d70-3f34-4c2e-a35f-2b837d750067
lastexecution/cpa-seq-validate-violation-witnesses/0fc94d70-3f34-4c2e-a35f-2b837d750067/output
lastexecution/cpa-seq-validate-violation-witnesses/0fc94d70-3f34-4c2e-a35f-2b837d750067/output/witness.graphml
lastexecution/cpa-seq-validate-violation-witnesses/0fc94d70-3f34-4c2e-a35f-2b837d750067/output.txt
lastexecution/cpachecker-predicate-craig-interpolation
lastexecution/cpachecker-predicate-craig-interpolation/420f72b4-e90d-40c9-b743-a12dcfff5c4e
lastexecution/cpachecker-predicate-craig-interpolation/420f72b4-e90d-40c9-b743-a12dcfff5c4e/output
lastexecution/cpachecker-predicate-craig-interpolation/420f72b4-e90d-40c9-b743-a12dcfff5c4e/output/witness.graphml
lastexecution/cpachecker-predicate-craig-interpolation/420f72b4-e90d-40c9-b743-a12dcfff5c4e/output.txt
lastexecution/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de
lastexecution/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output
lastexecution/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output/witness.graphml
lastexecution/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output/a927e869-46bf-455f-a0e0-1f789427747f.graphml
lastexecution/cpachecker-predicate-craig-interpolation/675799d5-7565-4c1f-ae71-f989f470c4de/output.txt
```

<br>

### **4.2 Creating new Configurations**

This section is intended for researchers and developers who want to develop
new C-CEGAR compositions based on this artifact.

To use an analysis tool in a C-CEGAR composition,
there must be an [actor definition in CoVeriTeam](https://gitlab.com/sosy-lab/software/coveriteam/-/tree/main/doc#actor-definition).
A list of existing actor definitions is available in `experiments/coveriteam/actors`.

As an example, we show how to change the feasibility checker:

1. Copy an existing C-CEGAR composition:

    ```
   $ cp experiments/coveriteam/examples/Component-based_CEGAR/cCegar-invariantWitness_cex-cpachecker_ref-cpachecker.cvt \
        experiments/coveriteam/examples/Component-based_CEGAR/cCegar-invariantWitness_cex-nitwit_ref-cpachecker.cvt
    ```

    This creates the new composition file `experiments/coveriteam/examples/Component-based_CEGAR/cCegar-invariantWitness_cex-nitwit_ref-cpachecker.cvt`.


2. Choose a tool that can serve as feasibility checker.
We choose the tool [`NitWit`](https://github.com/moves-rwth/nitwit-validator), an interpretation-based violation-witness validator.
NitWit has the actor definition `experiments/coveriteam/actors/nitwit.yml`.

3. Update the created composition file to use NitWit.
   To do this, replace the following line (in `experiments/coveriteam/examples/Component-based_CEGAR/cCegar-invariantWitness_cex-nitwit_ref-cpachecker.cvt`):
   ```
   witness_validator
     = ActorFactory.create(ProgramValidator, "actors/cpa-validate-violation-witnesses.yml");
   ```

   with the chosen actor definition:

   ```
   witness_validator
     = ActorFactory.create(ProgramValidator, "../../actors/nitwit.yml")
   ```

4. Test your new composition:

   *Note:
   Tools known to CoVeriTeam will be automatically downloaded, so this
   step requires internet access.*
   
   ```
   $ cd experiments/coveriteam
   $ bin/coveriteam \
        examples/Component-based_CEGAR/cCegar-invariantWitness_cex-nitwit-cpachecker.cvt \
        --data-model ILP32 \
        --input specification_path=../sv-benchmarks/c/properties/unreach-call.prp \
        --input program_path=../sv-benchmarks/c/bitvector/jain_1-1.c

   ```

#### **Details**

If you want to change the abstract model explorer that is used, update the line `verifier = ActorFactory.create(ProgramValidator, "actors/cpa-predicate-NoCegar.yml");`
with the actor-definition .yml of your choice.

If you want to change the refiner that is used, update the line `refiner = ActorFactory.create(ProgramValidator, "actors/cpa-predicate-craig-interpolation.yml");`
with the actor-definition .yml of your choice.

The feasibility checker must be a violation-witness validator.
The verifier (abstract model explorer) and refiner must be correctness-witness validators.
You can get an overview of validators on [the SV-COMP webpage](https://sv-comp.sosy-lab.org/2022/systems.php):
in Section 'Validators' the violation-witness validators have type 'violation',
and the correctness-witness validators have type 'correctness'.

A refiner should also produce correctness witnesses with helpful invariants,
so that program analysis can make progress.

If you want to add a new tool to CoVeriTeam, consider CoVeriTeam's documentation
at `experiments/coveriteam/doc/index.md`.

---

## **5. Supplements**

The concrete tools used by CoVeriTeam are stored in `experiments/coveriteam/used-tools/tools`.

Directory `supplements/` contains two files, backing claims made in our article:

1. [`supplements/overview-verifiers-invariant-generation.html`](supplements/overview-verifiers-invariant-generation.html)
  shows an evaluation of verifiers that participated in SV-COMP'21. The goal of this evaluation
  is to select verifiers that produce meaningful invariant witnesses.
  Based on this data, Ultimate Automizer is the only reasonable selection, next to CPAchecker.

2. [`supplements/reasoning-predicates-fig3a.pdf`](supplements/reasoning-predicates-fig3a.pdf)
  shows formal reasoning about the predicates shown in Figure 3a of our article:
  it shows that both predicates are equivalent.
